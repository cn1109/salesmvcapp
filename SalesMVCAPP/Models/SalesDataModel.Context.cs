﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SalesMVCAPP.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class SalesDataEntities : DbContext
    {
        public SalesDataEntities()
            : base("name=SalesDataEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<salesData> salesDatas { get; set; }
    
        public virtual ObjectResult<Daily_Result> Daily(Nullable<System.DateTime> sDate, Nullable<System.DateTime> eDate)
        {
            var sDateParameter = sDate.HasValue ?
                new ObjectParameter("sDate", sDate) :
                new ObjectParameter("sDate", typeof(System.DateTime));
    
            var eDateParameter = eDate.HasValue ?
                new ObjectParameter("eDate", eDate) :
                new ObjectParameter("eDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Daily_Result>("Daily", sDateParameter, eDateParameter);
        }
    
        public virtual ObjectResult<Monthly_Result> Monthly(Nullable<System.DateTime> sDate, Nullable<System.DateTime> eDate)
        {
            var sDateParameter = sDate.HasValue ?
                new ObjectParameter("sDate", sDate) :
                new ObjectParameter("sDate", typeof(System.DateTime));
    
            var eDateParameter = eDate.HasValue ?
                new ObjectParameter("eDate", eDate) :
                new ObjectParameter("eDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Monthly_Result>("Monthly", sDateParameter, eDateParameter);
        }
    
        public virtual ObjectResult<Quaterly_Result> Quaterly(Nullable<System.DateTime> sDate, Nullable<System.DateTime> eDate)
        {
            var sDateParameter = sDate.HasValue ?
                new ObjectParameter("sDate", sDate) :
                new ObjectParameter("sDate", typeof(System.DateTime));
    
            var eDateParameter = eDate.HasValue ?
                new ObjectParameter("eDate", eDate) :
                new ObjectParameter("eDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Quaterly_Result>("Quaterly", sDateParameter, eDateParameter);
        }
    
        public virtual ObjectResult<Weekly_Result> Weekly(Nullable<System.DateTime> sDate, Nullable<System.DateTime> eDate)
        {
            var sDateParameter = sDate.HasValue ?
                new ObjectParameter("sDate", sDate) :
                new ObjectParameter("sDate", typeof(System.DateTime));
    
            var eDateParameter = eDate.HasValue ?
                new ObjectParameter("eDate", eDate) :
                new ObjectParameter("eDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Weekly_Result>("Weekly", sDateParameter, eDateParameter);
        }
    }
}
