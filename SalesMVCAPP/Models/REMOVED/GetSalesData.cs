﻿using System;
using System.Linq;

namespace SalesMVCAPP.Models
{
    public class GetSalesData
    {
        public IQueryable<salesData> GetSalesByMonth()
        {
            IQueryable<salesData> results;

            using (var entity = new SalesDataEntities())
            {
                var startDate = Convert.ToDateTime("2015-04-17");
                var endDate = Convert.ToDateTime("2015-10-17");

                results = entity.salesDatas.Where(x => x.transactionDate > startDate && x.transactionDate < endDate);
               
            }

            return results;

        }
    }
}