﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SalesMVCAPP.Models;

namespace SalesMVCAPP.Controllers
{
    public class MainController : Controller
    {
        internal class ChartFormat
        {
            public DateTime? TransactionDate { get; set; }
            public decimal? SalesCount { get; set; }
            public decimal? SalesTotal { get; set; }
        }

        internal class ChartFormatCustom
        {
            public string TransactionDate { get; set; }
            public decimal? SalesCount { get; set; }
            public decimal? SalesTotal { get; set; }
        }

        

        //
        // GET: /Main/

        public ActionResult Index()
        {

            return View();
        }


        [HttpPost]
        public string GetSalesByDay(FormCollection collection, string startDate, string endDate)
        {
            using (var entity = new SalesDataEntities())
            {
                var sDate = Convert.ToDateTime(startDate);
                var eDate = Convert.ToDateTime(endDate);

                var results =
                    entity.Daily(sDate, eDate)
                        .Select(x => new ChartFormatCustom
                        {
                            SalesCount = x.totalSales,
                            SalesTotal = x.total,
                            TransactionDate = Convert.ToDateTime(x.transactionDate).ToString("yyyy/MM/dd")
                         });

                var json = JsonConvert.SerializeObject(results.ToList(), Formatting.None);
                return json;
            }
        }

        [HttpPost]
        public string GetSalesByMonth(FormCollection collection, string startDate, string endDate)
        {

            using (var entity = new SalesDataEntities())
            {
                var sDate = Convert.ToDateTime(startDate);
                var eDate = Convert.ToDateTime(endDate);

                var results =
                    entity.Monthly(sDate, eDate)
                        .Select(x => new ChartFormatCustom
                        {
                            SalesCount = x.totalSales,
                            SalesTotal = x.total,
                            TransactionDate = x.Month_Name
                        });

                var json = JsonConvert.SerializeObject(results.ToList(), Formatting.None);
                return json;

            }
        }

        [HttpPost]
        public string GetSalesQuarterly(FormCollection collection, string startDate, string endDate)
        {
            using (var entity = new SalesDataEntities())
            {
                var sDate = Convert.ToDateTime(startDate);
                var eDate = Convert.ToDateTime(endDate);

                var results =
                    entity.Quaterly(sDate, eDate)
                        .Select(x => new ChartFormatCustom
                        {
                            SalesCount = x.totalSales,
                            SalesTotal = x.total,
                            TransactionDate = x.Quarter.ToString()
                        });

                var json = JsonConvert.SerializeObject(results.ToList(), Formatting.None);
                return json;
            }
        }

        [HttpPost]
        public string GetSalesWeekly(FormCollection collection, string startDate, string endDate)
        {
            using (var entity = new SalesDataEntities())
            {
                var sDate = Convert.ToDateTime(startDate);
                var eDate = Convert.ToDateTime(endDate);

                var results =
                    entity.Weekly(sDate, eDate)
                        .Select(x => new ChartFormatCustom
                        {
                            SalesCount = x.totalSales,
                            SalesTotal = x.total,
                            TransactionDate = x.Week.ToString()
                        });

                var json = JsonConvert.SerializeObject(results.ToList(), Formatting.None);
                return json;
            }
        }
    
       
    }
}
