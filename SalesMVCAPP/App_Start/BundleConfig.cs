﻿using System;
using System.Web.Optimization;

namespace SalesMVCAPP.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scriptBundle")
             .Include("~/JS/Base/d3.min.js")
             .Include("~/JS/Base/c3.min.js")
             .Include("~/JS/Base/angular.js")
             .Include("~/JS/Base/ui-bootstrap-tpls-0.14.3.min.js")
             .Include("~/bower_components/c3-angular/c3-angular.min.js")
             .Include("~/bower_components/c3-angular/c3js-directive.js")
             .IncludeDirectory("~/JS/Controllers", "*.js")
             );
            
            bundles.Add(new StyleBundle("~/bundles/styleBundle")
                .Include("~/CSS/c3.min.css")
                .Include("~/CSS/bootstrap.min.css"));

            BundleTable.EnableOptimizations = false;
        }
    }
}