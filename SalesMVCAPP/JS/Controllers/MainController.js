﻿


var App = angular.module('SalesMVCAPP', ['gridshore.c3js.chart', 'SalesMVCAppService']);

App.controller('GraphCtrl', function ($scope, $interval, dataService) {

    $scope.pass = false;

    $scope.sDate = new Date("04/17/2015");
    $scope.eDate = new Date("12/17/2015");

    $scope.dropDownData = {

        availableOptions: [
          { id: '1', name: 'Quarter' },
          { id: '2', name: 'Month' },
          { id: '3', name: 'Week' },
          { id: '4', name: 'Day' }
        ],
        selectedOption: { id: '1', name: 'Quarter' } //This sets the default value of the select in the ui


    };

    $scope.setDropDown = function() {
        $scope.showGraph();
    };

    $scope.setStartDate = function() {
        console.log($scope.sDate);
        $scope.showGraph();

    };

    $scope.setEndDate = function() {
        console.log($scope.eDate);
        $scope.showGraph();

    };

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.showGraph = function () {

        

        //Regression temp bug fix.
        $scope.time_format = function(t) { return d3.time.format("%Y")(new Date(t)); };

        $scope.datapoints = [];

        $scope.datacolumns = [{ "id": "SalesCount", "type": "bar", "name": "Sales" },
                            { "id": "SalesTotal", "type": "line", "name": "Sum $" }];

        $scope.datax = { "id": "TransactionDate" };

        switch ($scope.dropDownData.selectedOption["name"]) {
            case "Quarter":
                
                dataService.loadQuaterlyData($scope.sDate, $scope.eDate, function (data) {
                    $scope.datapoints = data;
                    console.log(data);
                });
                $scope.pass = true;
                break;

            case "Week":
                
                //FIX: C3 Does not allow dynamic axisType change.
                dataService.loadWeeklyData($scope.sDate, $scope.eDate, function (data) {
                    $scope.datapoints = data;
                });
                $scope.pass = true;
                break;

            case "Day":

                dataService.loadDailyData($scope.sDate, $scope.eDate, function (data) {
                    $scope.datapoints = data;
                });

                break;
                
            case "Month":

                dataService.loadMonthlyData($scope.sDate, $scope.eDate, function (data) {
                    $scope.datapoints = data;
                });
                $scope.pass = true;
                break;

            default:


                dataService.loadDailyData($scope.sDate, $scope.eDate, function (data) {
                    $scope.datapoints = data;
                });
                $scope.pass = true;
                break;

        }

       
    };

    $scope.showGraph();
});

var services = angular.module('SalesMVCAppService', []);
services.factory('dataService', function ($http) {

    function dataService() {
       

        this.loadDailyData = function(sDate, eDate, callback) {

            $http.post("/Main/GetSalesByDay", { startDate: sDate, endDate: eDate }).then(function(response) {
                var data = response.data;
                callback(data);
            });

        };

        this.loadMonthlyData= function (sDate, eDate, callback) {

            $http.post("/Main/GetSalesByMonth", { startDate: sDate, endDate: eDate }).then(function (response) {
                var data = response.data;
                callback(data);
            });
        };

        this.loadWeeklyData = function (sDate, eDate, callback) {
            
            $http.post("/Main/GetSalesWeekly", { startDate: sDate, endDate: eDate }).then(function (response) {
                var data = response.data;
                callback(data);
            });
        };

        this.loadQuaterlyData = function(sDate, eDate, callback) {

            $http.post("/Main/GetSalesQuarterly", { startDate: sDate, endDate: eDate }).then(function(response) {
                var data = response.data;
                callback(data);
            });

        };

      
    }

    return new dataService();


});
