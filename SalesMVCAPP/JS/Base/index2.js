var graphApp = angular.module('graphApp', []);
graphApp.controller('GraphCtrl', function ($scope) {
    $scope.chart = null;
		
	$scope.showGraph = function() {
		
		$scope.chart = null;
		$scope.config={};
		
		//Data Configuration
		$scope.config.data1="30, 200, 100, 200, 150, 250";
		$scope.config.data2="70, 30, 10, 240, 150, 125";
		//$scope.config.data3="70, 30, 10, 240, 150, 125";
	 
		$scope.typeOptions=["line","bar","spline","step","area","area-step","area-spline"];
	 
		//Chart Display options
		$scope.config.type1=$scope.typeOptions[0];
		$scope.config.type2=$scope.typeOptions[1];
		//$scope.config.type3=$scope.typeOptions[4];
		
        var config = {};
        config.bindto = '#chart';
        config.data = {};
        
		//Assign the data
		config.data.json = {};
        config.data.json.data1 = $scope.config.data1.split(",");
        config.data.json.data2 = $scope.config.data2.split(",");
		//config.data.json.data3 = $scope.config.data3.split(",");
		
        config.axis = {"y":{"label":{"text":"Number of items","position":"outer-middle"}}};
		
        config.data.types = { "data1": $scope.config.type1, "data2": $scope.config.type2 };
        //config.data.types={"data1":$scope.config.type1,"data2":$scope.config.type2, "data3":$scope.config.type3};
		//config.data.colors = {"data1" :  "black", "data2" :  "blue", "data3" :  "purple"};
		
		config.zoom = {"enabled" : true};
		config.legend = {"position" : 'right'};
		config.subchart = {"show" : true};
		
		//Putting everything together
        $scope.chart = c3.generate(config);    

		
    }
});