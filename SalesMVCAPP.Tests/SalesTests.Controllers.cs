﻿using System;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using SalesMVCAPP.Controllers;

namespace SalesMVCAPP.Tests
{
    [TestClass]
    public class SalesTests
    {

        [TestMethod]
        public void TestSalesByQuarter()
        {
            var controller = new MainController();
            var result = controller.GetSalesQuarterly(new FormCollection(), "04/17/2015", "06/17/2015") as String;
            VerifyJsonFormat(result);
        }

        [TestMethod]
        public void TestSalesByWeek()
        {
            var controller = new MainController();
            var result = controller.GetSalesWeekly(new FormCollection(), "04/17/2015", "06/17/2015") as String;
            VerifyJsonFormat(result);
        }

        [TestMethod]
        public void TestSalesByDay()
        {
            var controller = new MainController();
            var result = controller.GetSalesByDay(new FormCollection(), "04/17/2015", "06/17/2015") as String;
            VerifyJsonFormat(result);
        }

        [TestMethod]
        public void TestSalesByMonth()
        {
            var controller = new MainController();
            var result = controller.GetSalesByMonth(new FormCollection(), "04/17/2015", "06/17/2015") as String;
            VerifyJsonFormat(result);
        }

        public void VerifyJsonFormat(string result)
        {
            try
            {
                var jobj = JToken.Parse(result);
            }
            catch (Exception e)
            {
                throw new Exception("Error: " + e);
            }
        }

    }
}
