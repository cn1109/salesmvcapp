﻿/// <reference path="../scripts/jasmine.js" />

/// <reference path="../../SalesMVCAPP/bower_components/d3/d3.js" />
/// <reference path="../../SalesMVCAPP/bower_components/c3/c3.js" />

/// <reference path="../../SalesMVCAPP/JS/Base/angular.js" />
/// <reference path="../../SalesMVCAPP/JS/Base/angular-mocks.js" />

/// <reference path="../../SalesMVCAPP/bower_components/c3-angular/c3-angular.min.js" />
/// <reference path="../../SalesMVCAPP/bower_components/c3-angular/c3js-directive.js" />

/// <reference path="../../SalesMVCAPP/JS/Controllers/MainController.js" />

describe("AngularTest", function () {


    beforeEach(module('SalesMVCAPP'));

    describe("Chart-DDL-", function () {

        var scope;
        beforeEach(inject(function ($rootScope, $controller, dataService) {
            scope = $rootScope.$new();
            $controller("GraphCtrl", {
                $scope: scope
            });
        }));

        it("it should set option to Quarter to call proper service", function () {

            scope.dropDownData.selectedOption["name"] = "Quarter";
            scope.showGraph();

            expect(scope.pass).toBe(true);
        });
        

        it("it should set option to Week to call proper service", function () {

            scope.dropDownData.selectedOption["name"] = "Week";
            scope.showGraph();

            expect(scope.pass).toBe(true);
        });
        

        it("it should set option to Day to call proper service", function () {

            scope.dropDownData.selectedOption["name"] = "Day";
            scope.showGraph();

            expect(scope.pass).toBe(true);
        });
        

        it("it should set option to Month to call proper service", function () {

            scope.dropDownData.selectedOption["name"] = "Month";
            scope.showGraph();
            console.log(scope.pass);

            expect(scope.pass).toBe(true);
        });

    });
    
    
    
});